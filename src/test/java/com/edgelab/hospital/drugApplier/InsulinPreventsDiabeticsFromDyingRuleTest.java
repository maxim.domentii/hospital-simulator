package com.edgelab.hospital.drugApplier;

import com.edgelab.hospital.model.Drug;
import com.edgelab.hospital.model.PatientState;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class InsulinPreventsDiabeticsFromDyingRuleTest {

    private InsulinPreventsDiabeticsFromDyingRule rule = new InsulinPreventsDiabeticsFromDyingRule();

    @Test
    void testApplyForZeroPatients() {
        //given
        Map<PatientState, Long> patients = new HashMap<>();
        Set<Drug> drugs = new HashSet<>();

        //when
        rule.apply(patients, drugs);

        //then
        assertTrue(patients.isEmpty());
    }

    @Test
    void testApplyWhenNoRuleExpectedDrugsGiven() {
        //given
        Map<PatientState, Long> patients = new HashMap<>();
        patients.put(PatientState.H, 1L);
        patients.put(PatientState.F, 1L);
        patients.put(PatientState.D, 1L);
        patients.put(PatientState.T, 1L);
        Set<Drug> drugs = new HashSet<>();

        //when
        rule.apply(patients, drugs);

        //then
        assertEquals(4, patients.size());
        assertFalse(patients.containsKey(PatientState.D));
        assertTrue(patients.containsKey(PatientState.X));
        assertEquals(1, patients.get(PatientState.X));
    }

    @Test
    void testApplyWhenRuleExpectedDrugsGiven() {
        //given
        Map<PatientState, Long> patients = new HashMap<>();
        patients.put(PatientState.H, 1L);
        patients.put(PatientState.F, 1L);
        patients.put(PatientState.D, 1L);
        patients.put(PatientState.T, 1L);
        Set<Drug> drugs = new HashSet<>(Collections.singletonList(Drug.I));

        //when
        rule.apply(patients, drugs);

        //then
        assertEquals(4, patients.size());
        assertFalse(patients.containsKey(PatientState.X));
        assertTrue(patients.containsKey(PatientState.D));
        assertEquals(1, patients.get(PatientState.D));
    }
}