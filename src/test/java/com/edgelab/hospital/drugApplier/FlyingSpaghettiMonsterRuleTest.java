package com.edgelab.hospital.drugApplier;

import com.edgelab.hospital.model.Drug;
import com.edgelab.hospital.model.PatientState;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class FlyingSpaghettiMonsterRuleTest {

    private FlyingSpaghettiMonsterRule rule = new FlyingSpaghettiMonsterRule();

    /*
    * Because it is a probabilistic problem it may happen that sometimes 2 subjects from one million will be resurrected
    * other times no one from one million. The median will me always one in a million. So I have chosen to test few
    * chances from 10 millions just to be sure that test has minimum chances to fail.
    * */
    @Test
    void apply() {
        //given
        Map<PatientState, Long> patients = new HashMap<>();
        patients.put(PatientState.X, 10000000L);
        Set<Drug> drugs = new HashSet<>();

        //when
        rule.apply(patients, drugs);

        //then
        assertEquals(2, patients.size());
        assertTrue(patients.containsKey(PatientState.H));
        assertTrue(patients.get(PatientState.H) >= 1);
        assertTrue(patients.containsKey(PatientState.X));
        assertTrue(patients.get(PatientState.X) <= 9999999);
    }
}