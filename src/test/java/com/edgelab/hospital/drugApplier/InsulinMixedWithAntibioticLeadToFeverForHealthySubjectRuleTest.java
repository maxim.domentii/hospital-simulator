package com.edgelab.hospital.drugApplier;

import com.edgelab.hospital.model.Drug;
import com.edgelab.hospital.model.PatientState;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class InsulinMixedWithAntibioticLeadToFeverForHealthySubjectRuleTest {

    private InsulinMixedWithAntibioticLeadToFeverForHealthySubjectRule rule =
            new InsulinMixedWithAntibioticLeadToFeverForHealthySubjectRule();

    @Test
    void testApplyForZeroPatients() {
        //given
        Map<PatientState, Long> patients = new HashMap<>();
        Set<Drug> drugs = new HashSet<>();

        //when
        rule.apply(patients, drugs);

        //then
        assertTrue(patients.isEmpty());
    }

    @Test
    void testApplyWhenNoRuleExpectedDrugsGiven() {
        //given
        Map<PatientState, Long> patients = new HashMap<>();
        patients.put(PatientState.H, 2L);
        Set<Drug> drugs = new HashSet<>();

        //when
        rule.apply(patients, drugs);

        //then
        assertEquals(1, patients.size());
        assertTrue(patients.containsKey(PatientState.H));
        assertEquals(2, patients.get(PatientState.H));
    }

    @Test
    void testApplyWhenRuleExpectedDrugsGiven() {
        //given
        Map<PatientState, Long> patients = new HashMap<>();
        patients.put(PatientState.H, 3L);
        Set<Drug> drugs = new HashSet<>(Arrays.asList(Drug.I, Drug.An));

        //when
        rule.apply(patients, drugs);

        //then
        assertEquals(1, patients.size());
        assertTrue(patients.containsKey(PatientState.F));
        assertEquals(3, patients.get(PatientState.F));
    }
}