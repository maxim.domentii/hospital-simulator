package com.edgelab.hospital.parser;

import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class InputPatientStatesParserTest {

    private InputPatientStatesParser parser = new InputPatientStatesParser();

    @Test
    void apply() {
        Map<String, Long> result = parser.apply(null);
        assertNotNull(result);
        assertTrue(result.isEmpty());

        result = parser.apply("");
        assertNotNull(result);
        assertTrue(result.isEmpty());

        result = parser.apply(",");
        assertNotNull(result);
        assertTrue(result.isEmpty());

        result = parser.apply("A,");
        assertNotNull(result);
        assertEquals(1, result.size());

        result = parser.apply("A,B");
        assertNotNull(result);
        assertEquals(2, result.size());

        result = parser.apply("A,A");
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(2, result.get("A"));
    }
}