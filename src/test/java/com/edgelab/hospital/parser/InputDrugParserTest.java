package com.edgelab.hospital.parser;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class InputDrugParserTest {

    private InputDrugsParser parser = new InputDrugsParser();

    @Test
    void apply() {
        Set<String> result = parser.apply(null);
        assertNotNull(result);
        assertTrue(result.isEmpty());

        result = parser.apply("");
        assertNotNull(result);
        assertTrue(result.isEmpty());

        result = parser.apply(",");
        assertNotNull(result);
        assertTrue(result.isEmpty());

        result = parser.apply("A,");
        assertNotNull(result);
        assertEquals(1, result.size());

        result = parser.apply("A,B");
        assertNotNull(result);
        assertEquals(2, result.size());

        result = parser.apply("A,A");
        assertNotNull(result);
        assertEquals(1, result.size());
    }
}