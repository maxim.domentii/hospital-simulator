package com.edgelab.hospital.parser;

import com.edgelab.hospital.model.PatientState;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class PatientStatesToStringParserTest {

    private PatientStatesToStringParser parser = new PatientStatesToStringParser();

    @Test
    void apply() {
        String result = parser.apply(null);
        assertNotNull(result);
        assertEquals("F:0,H:0,D:0,T:0,X:0", result);

        Map<PatientState, Long> input = new HashMap<>();
        result = parser.apply(input);
        assertNotNull(result);
        assertEquals("F:0,H:0,D:0,T:0,X:0", result);

        input.put(PatientState.X, 1L);
        result = parser.apply(input);
        assertNotNull(result);
        assertEquals("F:0,H:0,D:0,T:0,X:1", result);

        input.put(PatientState.T, 2L);
        result = parser.apply(input);
        assertNotNull(result);
        assertEquals("F:0,H:0,D:0,T:2,X:1", result);

        input.put(PatientState.D, 3L);
        result = parser.apply(input);
        assertNotNull(result);
        assertEquals("F:0,H:0,D:3,T:2,X:1", result);

        input.put(PatientState.H, 5L);
        result = parser.apply(input);
        assertNotNull(result);
        assertEquals("F:0,H:5,D:3,T:2,X:1", result);

        input.put(PatientState.F, 4L);
        result = parser.apply(input);
        assertNotNull(result);
        assertEquals("F:4,H:5,D:3,T:2,X:1", result);
    }
}