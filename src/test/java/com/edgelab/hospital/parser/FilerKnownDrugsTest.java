package com.edgelab.hospital.parser;

import com.edgelab.hospital.model.Drug;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class FilerKnownDrugsTest {

    private FilerKnownDrugs filter = new FilerKnownDrugs();

    @Test
    void apply() {
        Set<Drug> result = filter.apply(null);
        assertNotNull(result);
        assertTrue(result.isEmpty());

        Set<String> input = new HashSet<>();
        result = filter.apply(input);
        assertNotNull(result);
        assertTrue(result.isEmpty());

        input.add("A");
        result = filter.apply(input);
        assertNotNull(result);
        assertTrue(result.isEmpty());

        input.add("As");
        input.add("As");
        result = filter.apply(input);
        assertNotNull(result);
        assertEquals(1, result.size());

        input.add("An");
        result = filter.apply(input);
        assertNotNull(result);
        assertEquals(2, result.size());

        input.add("I");
        result = filter.apply(input);
        assertNotNull(result);
        assertEquals(3, result.size());

        input.add("P");
        result = filter.apply(input);
        assertNotNull(result);
        assertEquals(4, result.size());
    }
}