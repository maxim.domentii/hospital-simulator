package com.edgelab.hospital.parser;

import com.edgelab.hospital.model.PatientState;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class FilerKnownPatientStatesTest {

    private FilerKnownPatientStates filter = new FilerKnownPatientStates();

    @Test
    void apply() {
        Map<PatientState, Long> result = filter.apply(null);
        assertNotNull(result);
        assertTrue(result.isEmpty());

        Map<String, Long> input = new HashMap<>();
        result = filter.apply(input);
        assertNotNull(result);
        assertTrue(result.isEmpty());

        input.put("A", 1L);
        result = filter.apply(input);
        assertNotNull(result);
        assertTrue(result.isEmpty());

        input.put("H", 2L);
        result = filter.apply(input);
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(2, result.get(PatientState.H));

        input.put("F", 3L);
        result = filter.apply(input);
        assertNotNull(result);
        assertEquals(2, result.size());
        assertEquals(3, result.get(PatientState.F));

        input.put("D", 4L);
        result = filter.apply(input);
        assertNotNull(result);
        assertEquals(3, result.size());
        assertEquals(4, result.get(PatientState.D));

        input.put("T", 5L);
        result = filter.apply(input);
        assertNotNull(result);
        assertEquals(4, result.size());
        assertEquals(5, result.get(PatientState.T));

        input.put("X", 0L);
        result = filter.apply(input);
        assertNotNull(result);
        assertEquals(5, result.size());
        assertEquals(0, result.get(PatientState.X));
    }
}