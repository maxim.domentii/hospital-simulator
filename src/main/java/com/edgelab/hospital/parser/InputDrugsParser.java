package com.edgelab.hospital.parser;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InputDrugsParser implements Function<String, Set<String>> {

    private static final String SEPARATOR = ",";

    @Override
    public Set<String> apply(String s) {
        if (s == null) {
            return new HashSet<>();
        }
        return Stream.of(s.split(SEPARATOR))
                .map(String::trim)
                .filter(i -> !i.isEmpty())
                .collect(Collectors.toSet());
    }
}
