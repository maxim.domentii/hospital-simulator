package com.edgelab.hospital.parser;

import com.edgelab.hospital.model.Drug;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FilerKnownDrugs implements Function<Set<String>, Set<Drug>> {
    @Override
    public Set<Drug> apply(Set<String> drugsSet) {
        if (drugsSet == null) {
            return new HashSet<>();
        }
        return drugsSet.stream()
                .filter(e -> {
                    try {
                        Drug.valueOf(e);
                        return true;
                    } catch (IllegalArgumentException ex) {
                        System.err.println("Filtering out unknown drug: " + e);
                        return false;
                    }
                })
                .map(Drug::valueOf)
                .collect(Collectors.toSet());
    }
}
