package com.edgelab.hospital.parser;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InputPatientStatesParser implements Function<String, Map<String, Long>> {

    private static final String SEPARATOR = ",";

    @Override
    public Map<String, Long> apply(String s) {
        if (s == null) {
            return new HashMap<>();
        }
        return Stream.of(s.split(SEPARATOR))
                .map(String::trim)
                .filter(i -> !i.isEmpty())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }
}
