package com.edgelab.hospital.parser;

import com.edgelab.hospital.model.PatientState;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FilerKnownPatientStates implements Function<Map<String, Long>, Map<PatientState, Long>> {
    @Override
    public Map<PatientState, Long> apply(Map<String, Long> patientStatesMap) {
        if (patientStatesMap == null) {
            return new HashMap<>();
        }
        return patientStatesMap.entrySet().stream()
                .filter(e -> {
                    try {
                        PatientState.valueOf(e.getKey());
                        return true;
                    } catch (IllegalArgumentException ex) {
                        System.err.println("Filtering out unknown patient state: " + e.getKey());
                        return false;
                    }
                })
                .collect(Collectors.toMap(e -> PatientState.valueOf(e.getKey()), Map.Entry::getValue));
    }
}
