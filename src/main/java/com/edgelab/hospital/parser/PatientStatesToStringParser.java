package com.edgelab.hospital.parser;

import com.edgelab.hospital.model.PatientState;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class PatientStatesToStringParser implements Function<Map<PatientState, Long>, String> {

    private static final String DELIMITER = ",";

    @Override
    public String apply(Map<PatientState, Long> patientStatesMap) {
        if (patientStatesMap == null){
            patientStatesMap = new HashMap<>();
        }

        StringBuilder stringBuilder = new StringBuilder();
        for (PatientState patientState : PatientState.values()){
            stringBuilder.append(
                    String.format("%s:%s", patientState, patientStatesMap.getOrDefault(patientState, 0L)));
            stringBuilder.append(DELIMITER);
        }

        return stringBuilder.substring(0, stringBuilder.length() - 1);
    }
}
