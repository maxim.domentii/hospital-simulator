package com.edgelab.hospital.drugApplier;

import com.edgelab.hospital.model.Drug;
import com.edgelab.hospital.model.PatientState;

import java.util.Map;
import java.util.Set;

public abstract class AbstractTreatmentRuleApplier implements ITreatmentRuleApplier {

    protected ITreatmentRuleApplier nextRule;

    @Override
    public void setNextRule(ITreatmentRuleApplier nextRule) {
        this.nextRule = nextRule;
    }

    public void applyNextRuleIfExist(Map<PatientState, Long> patientStates, Set<Drug> drugs){
        if (this.nextRule != null){
            this.nextRule.apply(patientStates, drugs);
        }
    }
}
