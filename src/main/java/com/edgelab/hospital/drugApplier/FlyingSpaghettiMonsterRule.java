package com.edgelab.hospital.drugApplier;

import com.edgelab.hospital.model.Drug;
import com.edgelab.hospital.model.PatientState;

import java.util.Map;
import java.util.Set;
import java.util.SplittableRandom;

// 4th Order. Last rule to apply. In case there are dead subjects or someone dies because of treatment applied
// then there is a one in a million chances that the subject will be resurrected
public class FlyingSpaghettiMonsterRule extends AbstractTreatmentRuleApplier {

    /*
     * To simulate probability in Java, the first thing we need to do is to generate random numbers. Fortunately,
     * Java provides us with plenty of random numbers generators. In this case, we'll use the SplittableRandom class
     * because it provides high-quality randomness and is relatively fast.
     */
    private SplittableRandom random = new SplittableRandom();

    @Override
    public void apply(Map<PatientState, Long> patientStates, Set<Drug> drugs) {
        if (patientStates.containsKey(PatientState.X)){
            long countDeadPatients = patientStates.get(PatientState.X);
            long countResurrectedPatients = getResurrected(countDeadPatients);
            if (countResurrectedPatients > 0){
                patientStates.put(PatientState.X, countDeadPatients - countResurrectedPatients);
                patientStates.put(PatientState.H, countResurrectedPatients);
            }
        }

        applyNextRuleIfExist(patientStates, drugs);
    }

    /*
    * We need to generate a number in a range and compare it to another number chosen from that range. Every number
    * in the range has an equal chance of being drawn. As we know the range, we know the probability of drawing our
    * chosen number.
    * */
    private long getResurrected(long countOfDeadSubjects){
        long countOfResurrectedSubjects = 0L;
        for (int i=0; i<countOfDeadSubjects; i++){
            if (random.nextInt(1000000) == 0){
                countOfResurrectedSubjects++;
            }
        }
        return countOfResurrectedSubjects;
    }
}
