package com.edgelab.hospital.drugApplier;

import com.edgelab.hospital.model.Drug;
import com.edgelab.hospital.model.PatientState;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class DrugApplierManager {

    private ITreatmentRuleApplier treatmentRuleApplier;

    public DrugApplierManager(){
        treatmentRuleApplier = getTreatmentRuleApplier();
    }

    public Map<PatientState, Long> applyDrugs(Map<PatientState, Long> patientStates, Set<Drug> drugs){
        Map<PatientState, Long> resultStates = new HashMap<>(patientStates);

        treatmentRuleApplier.apply(resultStates, drugs);

        return resultStates;
    }

    private ITreatmentRuleApplier getTreatmentRuleApplier() {
        // 1st Order
        ITreatmentRuleApplier firstRule = new ParacetamolMixedWithAspirinKillsSubjectRule();

        // 2nd Order
        ITreatmentRuleApplier insulinMixedWithAntibioticRule =
                new InsulinMixedWithAntibioticLeadToFeverForHealthySubjectRule();
        firstRule.setNextRule(insulinMixedWithAntibioticRule);

        // 3rd Order
        ITreatmentRuleApplier insulinForDiabeticsRule = new InsulinPreventsDiabeticsFromDyingRule();
        insulinMixedWithAntibioticRule.setNextRule(insulinForDiabeticsRule);

        ITreatmentRuleApplier paracetamolForFeverRule = new ParacetamolCuresFeverRule();
        insulinForDiabeticsRule.setNextRule(paracetamolForFeverRule);

        ITreatmentRuleApplier aspirinForFeverRule = new AspirinCuresFeverRule();
        paracetamolForFeverRule.setNextRule(aspirinForFeverRule);

        ITreatmentRuleApplier antibioticsForTuberculosisRule = new AntibioticCuresTuberculosisRule();
        aspirinForFeverRule.setNextRule(antibioticsForTuberculosisRule);

        // 4th Order
        ITreatmentRuleApplier flyingSpaghettiMonsterRule = new FlyingSpaghettiMonsterRule();
        antibioticsForTuberculosisRule.setNextRule(flyingSpaghettiMonsterRule);

        return firstRule;
    }
}
