package com.edgelab.hospital.drugApplier;

import com.edgelab.hospital.model.Drug;
import com.edgelab.hospital.model.PatientState;

import java.util.Map;
import java.util.Set;

// 2nd Order. Second rule to apply. If given this drug combination then healthy catch fever
// and then if there is also paracetamol given then they become healthy again
public class InsulinMixedWithAntibioticLeadToFeverForHealthySubjectRule extends AbstractTreatmentRuleApplier {
    @Override
    public void apply(Map<PatientState, Long> patientStates, Set<Drug> drugs) {
        if (drugs.contains(Drug.I) && drugs.contains(Drug.An) && patientStates.containsKey(PatientState.H)){
            long countHealthyPatients = patientStates.get(PatientState.H);
            patientStates.remove(PatientState.H);
            patientStates.put(PatientState.F, countHealthyPatients);
        }

        applyNextRuleIfExist(patientStates, drugs);
    }
}
