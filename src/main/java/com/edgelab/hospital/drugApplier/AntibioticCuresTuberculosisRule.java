package com.edgelab.hospital.drugApplier;

import com.edgelab.hospital.model.Drug;
import com.edgelab.hospital.model.PatientState;

import java.util.Map;
import java.util.Set;

// 3rd Order. Third rule as priority.
public class AntibioticCuresTuberculosisRule extends AbstractTreatmentRuleApplier {
    @Override
    public void apply(Map<PatientState, Long> patientStates, Set<Drug> drugs) {
        if (drugs.contains(Drug.An) && patientStates.containsKey(PatientState.T)){
            long countPatientsWithTuberculosis = patientStates.get(PatientState.T);
            patientStates.remove(PatientState.T);
            patientStates.put(PatientState.H,
                    patientStates.getOrDefault(PatientState.H, 0L) + countPatientsWithTuberculosis);
        }

        applyNextRuleIfExist(patientStates, drugs);
    }
}
