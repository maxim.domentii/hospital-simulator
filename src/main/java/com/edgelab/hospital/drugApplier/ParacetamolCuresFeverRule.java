package com.edgelab.hospital.drugApplier;

import com.edgelab.hospital.model.Drug;
import com.edgelab.hospital.model.PatientState;

import java.util.Map;
import java.util.Set;

// 3rd Order. Third rule as priority.
public class ParacetamolCuresFeverRule extends AbstractTreatmentRuleApplier {
    @Override
    public void apply(Map<PatientState, Long> patientStates, Set<Drug> drugs) {
        if (drugs.contains(Drug.P) && patientStates.containsKey(PatientState.F)){
            long countPatientsWithFever = patientStates.get(PatientState.F);
            patientStates.remove(PatientState.F);
            patientStates.put(PatientState.H,
                    patientStates.getOrDefault(PatientState.H, 0L) + countPatientsWithFever);
        }

        applyNextRuleIfExist(patientStates, drugs);
    }
}
