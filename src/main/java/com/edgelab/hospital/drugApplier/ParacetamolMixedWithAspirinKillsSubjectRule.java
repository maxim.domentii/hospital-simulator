package com.edgelab.hospital.drugApplier;

import com.edgelab.hospital.model.Drug;
import com.edgelab.hospital.model.PatientState;

import java.util.Map;
import java.util.Set;

// 1st Order. First rule to apply. If this drug combination then all dies no matter of other drugs
public class ParacetamolMixedWithAspirinKillsSubjectRule extends AbstractTreatmentRuleApplier {
    @Override
    public void apply(Map<PatientState, Long> patientStates, Set<Drug> drugs) {
        if (drugs.contains(Drug.P) && drugs.contains(Drug.As)){
            long countPatients = patientStates.values().stream().reduce(Long::sum).orElse(0L);
            patientStates.clear();
            patientStates.put(PatientState.X, countPatients);
        }

        applyNextRuleIfExist(patientStates, drugs);
    }
}
