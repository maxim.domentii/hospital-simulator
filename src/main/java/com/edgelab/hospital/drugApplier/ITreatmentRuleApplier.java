package com.edgelab.hospital.drugApplier;

import com.edgelab.hospital.model.Drug;
import com.edgelab.hospital.model.PatientState;

import java.util.Map;
import java.util.Set;

public interface ITreatmentRuleApplier {

    void setNextRule(ITreatmentRuleApplier rule);
    void apply(Map<PatientState, Long> patientStates, Set<Drug> drugs);

}
