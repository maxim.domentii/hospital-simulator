package com.edgelab.hospital.model;

public enum PatientState {
    F, H, D, T, X
}
