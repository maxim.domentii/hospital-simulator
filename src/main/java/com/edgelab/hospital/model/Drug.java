package com.edgelab.hospital.model;

public enum Drug {
    As, An, I, P
}
