package com.edgelab.hospital;

import com.edgelab.hospital.drugApplier.DrugApplierManager;
import com.edgelab.hospital.model.PatientState;
import com.edgelab.hospital.parser.*;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Application {

    private static InputPatientStatesParser inputPatientStatesParser = new InputPatientStatesParser();
    private static InputDrugsParser inputDrugsParser = new InputDrugsParser();
    private static FilerKnownPatientStates filerKnownPatientStates = new FilerKnownPatientStates();
    private static FilerKnownDrugs filerKnownDrugs = new FilerKnownDrugs();
    private static PatientStatesToStringParser patientStatesToStringParser = new PatientStatesToStringParser();

    private static DrugApplierManager drugApplierManager = new DrugApplierManager();

    public static void main(String... args) {
        if (args.length < 1) {
            throw new IllegalArgumentException("Missing input arguments.\n" +
                    "Usage: java -cp <pathToJar>/hospital-simulator.jar com.edgelab.hospital.Application" +
                    " <listOfPatients> [<listOfDrugs>]");
        }

        Map<String, Long> patientsMap = inputPatientStatesParser.apply(args[0]);
        Set<String> drugsSet = new HashSet<>();
        if (args.length > 1){
            drugsSet = inputDrugsParser.apply(args[1]);
        }

        Map<PatientState, Long> resultStates = drugApplierManager.applyDrugs(
                filerKnownPatientStates.apply(patientsMap),
                filerKnownDrugs.apply(drugsSet)
        );

        System.out.println(patientStatesToStringParser.apply(resultStates));
    }
}
